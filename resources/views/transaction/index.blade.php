@extends ('layouts.main')

@section('content-top')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Form Transaksi</h3>
                </div>
                <div class="panel-body">
                    <form method="post" action="{{ route('transaction.store') }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            {{-- <fieldset disabled> --}}
                            <label for="inputNama">Nama Barang</label>
                            <select class="form-control" id="select" name="id_item[]" data-live-search="true" multiple
                                title="-- Pilih Barang --">
                                @foreach ($items as $item)
                                    <option value="{{ $item->id }}">{{ $item->nama_barang }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="inputUnit">Jumlah Barang</label>
                            <input type="text" class="form-control" id="inputUnit" placeholder="Masukan Unit"
                                name="jumlah">
                        </div>
                        <div class="form-group">
                            <label for="inputStock">Harga Satuan</label>
                            <select class="form-control" id="selectHarga" name="harga_satuan" data-live-search="true" multiple
                                title=" ">
                                @foreach ($items as $item)
                                    <option value="{{ $item->id }}">{{ $item->nama_barang }} - {{ $item->harga_satuan }}</option>
                                @endforeach
                                {{-- coba auto select cuman belum berhasil --}}
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" value="SIMPAN">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content-bottom')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Data Transaksi</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable-barang" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Waktu Transaksi</th>
                            <th>Total Harga</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            @foreach ($transactions as $tc)
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $tc->created_at }}</td>
                            <td>{{ $tc->total_harga }}</td>
                            @endforeach
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop

@section('script')
    <script>
        $(document).ready(function() {
            $('#dataTable-barang').DataTable();
        });
    </script>

    <script>
        $(function() {
            $('#select').selectpicker();
        });
    </script>

    <script>
        $(function() {
            $('#selectHarga').selectpicker();
        });
    </script>
    <!-- Page level plugins -->
@endsection

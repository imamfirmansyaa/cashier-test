@extends ('layouts.main')

@section('content-top')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Form Barang</h3>
                </div>
                <div class="panel-body">
                    <form method="post" action="{{ route('item.store') }}">
                        {{ csrf_field() }}


                        <div class="form-group">
                            {{-- <fieldset disabled> --}}
                            <label for="inputNama">Nama Barang</label>
                            <input type="text" class="form-control" id="inputNama" placeholder="Masukan Nama Barang"
                                name="nama_barang">
                        </div>
                        <div class="form-group">
                            {{-- <fieldset disabled> --}}
                            <label for="inputUnit">Harga Satuan</label>
                            <input type="text" class="form-control" id="inputUnit" placeholder="Masukan Harga"
                                name="harga_satuan">
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" value="SIMPAN">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content-bottom')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Data Barang</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable-barang" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Barang</th>
                            <th>Harga Satuan</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($items as $item)
                            <tr>
                                <td> {{ $loop->iteration }}</td>
                                <td>{{ $item->nama_barang }}</td>
                                <td>{{ $item->harga_satuan }}</td>
                                <td>
                                    <meta name="csrf-token" content="{{ csrf_token() }}">
                                    <button class="btn btn-success edit" value="{{$item->id}}"> Ubah</button>
                                    <button class="btn btn-danger drop" id="btn-confirm" value="{{$item->id}}">Hapus
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                </table>
            </div>
        </div>
    </div>
@stop

@section('script')
    <script>
        $(document).ready(function() {
            $('#dataTable-barang').DataTable();
        });
    </script>
    <!-- Page level plugins -->
@endsection

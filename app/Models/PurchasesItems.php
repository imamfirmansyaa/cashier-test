<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PurchasesItems extends Model
{
    use HasFactory;

    protected $fillable = ['id_purchases', 'id_items', 'jumlah', 'harga_satuan'];

    public function Item(){
        return $this->hasMany('App\Item', 'id_items', 'id');
    }

    public function Purchases(){
        return $this->belongsTo('App\Purchase', 'id_purchases', 'id');
    }
}

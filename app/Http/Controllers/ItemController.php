<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\Purchases;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::all();
        return view('items.index', [
            'items' => $items
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $message = [
            'nama_barang.required' => "Nama Barang Harus Diisi",
            'harga_satuan.required' => "Harga Barang Harus Diisi"
        ];
        $validator = Validator::make($request->all(), [
            'nama_barang' => 'required',
            'harga_satuan' => 'required'
        ], $message);

        if($validator->fails()){
            return redirect('item')->withErrors($validator)->withInput();
        }

        $item = new Item();
        $item->nama_barang = $request->nama_barang;
        $item->harga_satuan = $request->harga_satuan;

        $total = new Purchases();
        $total->total_barang = 0;

        $item->save();

        return redirect()->route('item.index')->with(['success' => "Data Berhasil Ditambahkan!"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\PurchaseItems;
use App\Models\Purchases;
use App\Models\PurchasesItems;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = PurchasesItems::all();
        $items = Item::orderBy('nama_barang', 'ASC')->get();
        return view('transaction.index', [
            'transactions' => $transactions,
            'items' => $items
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $messages = [
                'id_item.required' => 'Barang Harus Dipilih!',
                'jumlah.required' => 'Jumlah Harus Diisi!',
                'harga_satuan.required' => 'Harga Barang Harus Diisi'
            ];
            $validator = Validator::make($request->all(), [
                'id_item' =>  'required',
                'jumlah' => 'required',
                'harga_satuan' => 'required'
            ], $messages);

            $x = count($request->id_item);
            for ($i=0; $i < $x ; $i++) {
                $item = Item::find($request->id_item[$i]);

                $transaction = new PurchasesItems();
                $transaction->id_item = $item->id,
                $transaction->jumlah = $request->jumlah,
                $transaction->harga_satuan = $request->harga_satuan,
                $transaction->total_harga = $request->harga_satuan * $request->jumlah,
                $transaction->save();
            }
            if($validator->fails()){
                return redirect()->route('transaction.index')->withErrors('Transaksi Tidak Berhasil Ditambahkan!');
            }



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace Database\Seeders;

use App\Models\Item;
use Illuminate\Database\Seeder;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Item::create([
            'id' => 1,
            'nama_barang' => 'Sabun Batang',
            'harga_satuan' => 3000
        ]);

        Item::create([
            'id' => 2,
            'nama_barang' => 'Mi Instan',
            'harga_satuan' => 2000
        ]);
        Item::create([
            'id' => 3,
            'nama_barang' => 'Pensil',
            'harga_satuan' => 1000
        ]);

        Item::create([
            'id' => 4,
            'nama_barang' => 'Kopi Sachet',
            'harga_satuan' => 1500
        ]);

        Item::create([
            'id' => 5,
            'nama_barang' => 'Air Minum Galon',
            'harga_satuan' => 20000
        ]);
    }
}
